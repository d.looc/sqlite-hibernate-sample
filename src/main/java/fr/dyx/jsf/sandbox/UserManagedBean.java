package fr.dyx.jsf.sandbox;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import jakarta.enterprise.context.RequestScoped;
import jakarta.inject.Named;
import jakarta.persistence.Query;

@Named("userMBean")
@RequestScoped
public class UserManagedBean implements Serializable
{

    private static final long serialVersionUID = 1L;
    // private static Logger log = LogManager.getLogger(UserManagedBean.class);
    private static final String SUCCESS = "success";
    private static final String ERROR = "error";
    private String name;
    private String surname;
    private String message;
    private Integer count = 0;

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getSurname()
    {
        return surname;
    }

    public void setSurname(String surname)
    {
        this.surname = surname;
    }

    public String getMessage()
    {
        StringBuffer strBuff = new StringBuffer();
        strBuff.append("Name : ").append(this.getName());
        strBuff.append(", Surname : ").append(this.getSurname());
        this.setMessage(strBuff.toString());
        return this.message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public String save()
    {
        String result = null;
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        Session session = sessionFactory.openSession();

        User user = new User();
        user.setName(this.getName());
        user.setSurname(this.getSurname());

        Transaction tx = null;

        try
        {
            tx = session.beginTransaction();
            session.save(user);
            tx.commit();
            // log.debug("New Record : " + user + ", wasCommitted : " + tx.wasCommitted());
            result = SUCCESS;
        } catch (Exception e)
        {
            if (tx != null)
            {
                tx.rollback();
                result = ERROR;
                e.printStackTrace();
            }
        } finally
        {
            session.close();
        }
        return result;
    }
    
    @SuppressWarnings("unchecked")
	public Integer getCount()  
    {  
        Session session = HibernateUtil.getSessionFactory().openSession();  
        List < User > daoSearchList = new ArrayList < > (); 
        int result = count;
        try  
        {  
            session.beginTransaction();  
            Query qu = session.createQuery("From User U where U.name =:name"); //User is the entity not the table  
            qu.setParameter("name", "Louis");  
            daoSearchList = qu.getResultList();  
            result = daoSearchList.size();  
            session.getTransaction().commit();  
        }  
        catch (Exception e)  
        {  
            e.printStackTrace();  
            session.getTransaction().rollback();  
        }  
        finally  
        {  
            session.close();  
        }  
        return result;  
    }

    public void setCount(int i)
    {
    	this.count = i;
    }
    
    public List<User> getUsers()
    {
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<User> userList = session.createCriteria(User.class).list();
        return userList;
    }

    public void reset()
    {
        this.setName("");
        this.setSurname("");
    }
}