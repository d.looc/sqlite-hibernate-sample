package fr.dyx.jsf.sandbox;

import java.util.HashMap;
import java.util.Map;

import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.BootstrapServiceRegistry;
import org.hibernate.boot.registry.BootstrapServiceRegistryBuilder;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.cfg.beanvalidation.BeanValidationIntegrator;
import org.hibernate.service.ServiceRegistry;

/**
 * @author onlinetechvision.com
 * @since 3 Oct 2011
 * @version 1.0.0
 *
 */
public class HibernateUtil
{

    private static SessionFactory sessionFactory = null;
    private static StandardServiceRegistry registry;

    // https://hibernate.org/orm/documentation/getting-started/
    // https://learningprogramming.net/java/jsf/create-data-to-database-with-hibernate-in-jsf-framework/
    // https://www.c-sharpcorner.com/article/primefaces-5-and-hibernate-5-crud-using-netbeans-8-02-and-my/
    // https://stackoverflow.com/questions/52108690/how-to-fix-hibernate-configuration-error-and-exceptionininitializererror
    public static SessionFactory getSessionFactory()
    {
        if (sessionFactory == null)
        {
            /**Configuration configuration = new Configuration();
            configuration.configure();
            sessionFactory = configuration.buildSessionFactory();*/
            /**try {
            	BootstrapServiceRegistryBuilder bsrb = new BootstrapServiceRegistryBuilder();
                //BootstrapServiceRegistry bootstrapRegistry =
                //      new BootstrapServiceRegistryBuilder();
                bsrb = bsrb.applyIntegrator(new BeanValidationIntegrator());
                 
                BootstrapServiceRegistry bootstrapRegistry = bsrb.build();

                StandardServiceRegistryBuilder registryBuilder = 
                      new StandardServiceRegistryBuilder(bootstrapRegistry);

                Map<String, Object> settings = new HashMap<>();
                settings.put(Environment.DRIVER, "com.mysql.cj.jdbc.Driver");
                settings.put(Environment.URL, "jdbc:mysql://localhost:3306/springdata?useSSL=false");
                settings.put(Environment.USER, "root");
                settings.put(Environment.PASS, "pooja31");
                settings.put(Environment.HBM2DDL_AUTO, "update");
                settings.put(Environment.SHOW_SQL, true);


                registryBuilder.applySettings(settings);

                registry = registryBuilder.build();

                MetadataSources sources = new MetadataSources(registry)
                      .addAnnotatedClass( Employee.class)


                Metadata metadata = sources.getMetadataBuilder().build();

                sessionFactory = metadata.getSessionFactoryBuilder().build();
             } catch (Exception e) {
                if (registry != null) {
                   StandardServiceRegistryBuilder.destroy(registry);
                }
                e.printStackTrace();
             }*/
        	
        	 // Hibernate 5.4 SessionFactory example without XML
        	  Map<String, String> settings = new HashMap<>();
        	  settings.put("hibernate.connection.driver_class", "org.sqlite.JDBC");
        	  settings.put("hibernate.dialect", "org.sqlite.hibernate.dialect.SQLiteDialect");
        	  settings.put("hibernate.connection.url", 
        	    "jdbc:sqlite:db/sqlite/bank.db");
        	  settings.put("hibernate.connection.username", "");
        	  settings.put("hibernate.connection.password", "");
        	  settings.put("hibernate.current_session_context_class", "thread");
        	  settings.put("hibernate.show_sql", "true");
        	  settings.put("hibernate.format_sql", "true");

        	  ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
        	                                    .applySettings(settings).build();

        	  MetadataSources metadataSources = new MetadataSources(serviceRegistry);
        	  metadataSources.addAnnotatedClass(User.class);
        	  Metadata metadata = metadataSources.buildMetadata();

        	  // here we build the SessionFactory (Hibernate 5.4)
        	  sessionFactory = metadata.getSessionFactoryBuilder().build();

        }
        return sessionFactory;
    }

    public static void setSessionFactory(SessionFactory sessionFactory)
    {
        HibernateUtil.sessionFactory = sessionFactory;
    }

}
