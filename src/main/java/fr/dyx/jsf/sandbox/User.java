package fr.dyx.jsf.sandbox;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

/**
 * @author onlinetechvision.com
 * @since 3 Oct 2011
 * @version 1.0.0
 *
 */
@Entity
@Table(name = "USER")
public class User implements Serializable {
 

private static final long serialVersionUID = 1L;


private int id;

 
private String name;

 
private String surname;
 
@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
@Column(name = "id", unique = true, nullable = false) 
public int getId() {
  return id;
}
 
 public void setId(int id) {
  this.id = id;
 }
 
 @Column(name = "name", length = 45)
 public String getName() {
  return name;
 }
 
 public void setName(String name) {
  this.name = name;
 }
 
 @Column(name = "surname", length = 45)
 public String getSurname() {
  return surname;
 }
 
 public void setSurname(String surname) {
  this.surname = surname;
 } 
 
 @Override
 public String toString() {
  StringBuffer strBuff = new StringBuffer();
  strBuff.append("id : ").append(id);
  strBuff.append(", name : ").append(name);
  strBuff.append(", surname : ").append(surname);
  return strBuff.toString();
 }
}
